package net.domdiidom.jail.main;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.confuser.barapi.BarAPI;
import net.domdiidom.jail.commands.CommandJail;
import net.domdiidom.jail.commands.CommandUnJail;
import net.domdiidom.jail.events.EventBlockPlace;
import net.domdiidom.jail.events.EventItemUse;
import net.domdiidom.jail.events.EventPlayerDeath;
import net.domdiidom.jail.events.EventPlayerJoin;
import net.domdiidom.jail.events.EventPlayerMove;
import net.domdiidom.jail.utils.CreationState;
import net.domdiidom.jail.utils.Utils;

public class Main extends JavaPlugin{
	
	private ConsoleCommandSender console = Bukkit.getConsoleSender();
	public Utils utils;
	public CreationState state = CreationState.IDLE;
	public List<String> prisoners = new LinkedList<String>();
	
	@Override
	public void onEnable() {
		console.sendMessage("�2[Jail] Enabled");
		
		utils = new Utils(this);
				
		this.onLoadConfig();
		this.onRegisterCommands();
		this.onRegisterEvents();
		
		this.onUpdateJail();
	}
	
	@Override
	public void onDisable() {
		console.sendMessage("�4[Jail] Disabled");
	}
	
	private void onLoadConfig() {
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		this.onLoadPrisoners();
	}
	
	private void onLoadPrisoners() {
		prisoners = (List<String>) getConfig().getList("prisoners.list");
	}
	
	private void onRegisterCommands() {
		this.getCommand("jail").setExecutor(new CommandJail(this));
		this.getCommand("unjail").setExecutor(new CommandUnJail(this));
	}

	private void onRegisterEvents() {
		this.getServer().getPluginManager().registerEvents(new EventItemUse(this), this);
		this.getServer().getPluginManager().registerEvents(new EventBlockPlace(this), this);
		this.getServer().getPluginManager().registerEvents(new EventPlayerMove(this), this);
		this.getServer().getPluginManager().registerEvents(new EventPlayerDeath(this), this);
		this.getServer().getPluginManager().registerEvents(new EventPlayerJoin(this), this);
	}
	
	@SuppressWarnings("deprecation")
	private void onUpdateJail() {
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					if(getConfig().getString("prisoners."+player.getUniqueId()+".reason") != null && getConfig().getInt("prisoners."+player.getUniqueId()+".pause") == 0) {
						int current = getConfig().getInt("prisoners."+player.getUniqueId()+".current");
						int duration = getConfig().getInt("prisoners."+player.getUniqueId()+".duration");
						
						String reason = getConfig().getString("prisoners."+player.getUniqueId()+".reason");
							
						getConfig().set("prisoners."+player.getUniqueId()+".current", current+1);
						saveConfig();
						
						if(current >= duration) {
							utils.removePrisoner(player);
							int jail = getConfig().getInt("prisoners."+player.getUniqueId()+".jail");
							getConfig().set("jail."+jail+".prisoner", null);
							getConfig().set("jail."+jail+".occupied", 0);
							
							getConfig().set("prisoners."+player.getUniqueId(), null);
							saveConfig();
						}
						
						if((((duration-current)/60)) >=1) {
								BarAPI.setMessage(player, "Grund: " + reason+" | noch: "+(((duration-current)/60)+1)+" Minuten!");
						}
						else {
							BarAPI.setMessage(player, "Grund: " + reason+" | noch: "+(duration-current)+" Sekunden!");
						}
					}else {
						if(BarAPI.hasBar(player) && !utils.isCaptured(player)){
							BarAPI.removeBar(player);
						}
					}
				}
			}
		}, 0L, 20L);
	}
	
	@SuppressWarnings("deprecation")
	private void onUpdatePrisoner() {
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				
			}
		}, 0L, 0L);
	}
}
