package net.domdiidom.jail.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import net.domdiidom.jail.main.Main;

public class EventPlayerDeath implements Listener{
	Main main;
	public EventPlayerDeath(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onEvent(PlayerDeathEvent event) {
		if(main.utils.isCaptured(event.getEntity())) {
			int jail = main.utils.getJail(event.getEntity());
			
			main.getConfig().set("prisoners."+event.getEntity().getUniqueId()+".wolf", 0);
			main.saveConfig();
			
			int x = main.getConfig().getInt("jail."+jail+".spawn.x");
			int y = main.getConfig().getInt("jail."+jail+".spawn.y");
			int z = main.getConfig().getInt("jail."+jail+".spawn.z");
			
			event.getEntity().teleport(new Location(event.getEntity().getWorld(), x, y, z));
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		if(!main.utils.isCaptured(event.getPlayer()))
			return;
		
		main.getConfig().set("prisoners."+event.getPlayer().getUniqueId()+".pause", 0);
		main.saveConfig();
		
		int jail = main.utils.getJail(event.getPlayer());
				
		int x = main.getConfig().getInt("jail."+jail+".spawn.x");
		int y = main.getConfig().getInt("jail."+jail+".spawn.y");
		int z = main.getConfig().getInt("jail."+jail+".spawn.z");
		
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable() {
			@Override
			public void run() {
				event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), x, y, z));
			}
		}, 10L);
	}

}
