package net.domdiidom.jail.events;

import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.domdiidom.jail.main.Main;

public class EventPlayerJoin implements Listener{
	Main main;
	public EventPlayerJoin(Main main) {
		this.main = main;
	}
	
	public void onEvent(PlayerJoinEvent event) {

		if(!main.utils.isCaptured(event.getPlayer()))
			return;
		
		int jail = main.utils.getJail(event.getPlayer());
		
		int x = main.getConfig().getInt("jail."+jail+".spawn.x");
		int y = main.getConfig().getInt("jail."+jail+".spawn.y");
		int z = main.getConfig().getInt("jail."+jail+".spawn.z");
		
		event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), x, y, z));
	}
}
