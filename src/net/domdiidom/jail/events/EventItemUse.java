package net.domdiidom.jail.events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.domdiidom.jail.main.Main;
import net.domdiidom.jail.utils.CreationState;

public class EventItemUse implements Listener{
	Main main;
	public EventItemUse(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onEvent(PlayerInteractEvent event) {
		try {
			int id = main.utils.getNextJail();
			if(main.state == CreationState.SPAWN) {
				if(event.getItem().getItemMeta().getDisplayName().equals("Jail Creation Tool")) {
					
					main.getConfig().set("jail."+id+".spawn.x", event.getClickedBlock().getLocation().getBlockX());
					main.getConfig().set("jail."+id+".spawn.y", event.getClickedBlock().getLocation().getBlockY()+1);
					main.getConfig().set("jail."+id+".spawn.z", event.getClickedBlock().getLocation().getBlockZ());
					main.saveConfig();
					
					event.getPlayer().sendMessage("�3[JAIL] Jetzt den ausgang markieren!");
					main.state = CreationState.OUT;
				}
			}else if(main.state == CreationState.OUT) {
				if(event.getItem().getItemMeta().getDisplayName().equals("Jail Creation Tool")) {

					main.getConfig().set("jail."+id+".out.x", event.getClickedBlock().getLocation().getBlockX());
					main.getConfig().set("jail."+id+".out.z", event.getClickedBlock().getLocation().getBlockZ());
					
					main.getConfig().set("jail."+id+".occupied", 0);
										
					event.getPlayer().getInventory().setItem(0, new ItemStack(Material.CHEST, 2));
					event.getPlayer().sendMessage("�3[JAIL] Setze zum schluss noch die Kiste!");
					
					main.state = CreationState.CHEST;
				}
			}
		}catch(Exception e) {}
		
		if(event.getClickedBlock().getType() == Material.CHEST && main.utils.isCaptured(event.getPlayer())) {
			event.getPlayer().sendMessage("�4[JAIL] Du hast erst zugriff auf deine Items, wenn du entlassen wurdest!");
			event.setCancelled(true);
		}
	}
}
