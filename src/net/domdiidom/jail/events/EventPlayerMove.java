package net.domdiidom.jail.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import net.domdiidom.jail.main.Main;

public class EventPlayerMove implements Listener{
	Main main;
	public EventPlayerMove(Main main) {
		this.main = main;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEvent(PlayerMoveEvent event) {
		if(main.utils.isCaptured(event.getPlayer())) {
			int jail = main.utils.getJail(event.getPlayer());
			int x = main.getConfig().getInt("jail."+jail+".out.x");
			int z = main.getConfig().getInt("jail."+jail+".out.z");
			
			Location location = event.getPlayer().getLocation();			
			if(x == location.getBlockX() && z == location.getBlockZ() && main.getConfig().getInt("prisoners."+event.getPlayer().getUniqueId()+".wolf") == 0) {
				Location spawn = event.getPlayer().getLocation();
				Vector direction = spawn.getDirection().multiply(5);
				spawn.add(direction);
				spawn.setY(event.getPlayer().getLocation().getY());
				Wolf wolf1 = ((Wolf) event.getPlayer().getWorld().spawnEntity(spawn, EntityType.WOLF));
				Wolf wolf2 = ((Wolf) event.getPlayer().getWorld().spawnEntity(spawn, EntityType.WOLF));
				Wolf wolf3 = ((Wolf) event.getPlayer().getWorld().spawnEntity(spawn, EntityType.WOLF));
				wolf1.setTarget(event.getPlayer());
				wolf2.setTarget(event.getPlayer());
				wolf3.setTarget(event.getPlayer());
				wolf1.setAngry(true);
				wolf2.setAngry(true);
				wolf3.setAngry(true);
				
				main.getConfig().set("prisoners."+event.getPlayer().getUniqueId()+".wolf", 1);
				main.getConfig().set("prisoners."+event.getPlayer().getUniqueId()+".pause", 1);
				main.saveConfig();
				
				Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable() {
					@Override
					public void run() {
						while( main.getConfig().getInt("prisoners."+event.getPlayer().getUniqueId()+".wolf") == 1) {
							try {
							main.utils.moveEntity(wolf1, event.getPlayer().getLocation(), 2.5F);
							main.utils.moveEntity(wolf2, event.getPlayer().getLocation(), 2.5F);
							main.utils.moveEntity(wolf3, event.getPlayer().getLocation(), 2.5F);
							Thread.sleep(250);
							}catch(Exception e) {}
						}
						System.out.println("JETZT");
						wolf1.setHealth(0.0);
						wolf2.setHealth(0.0F);
						wolf3.setHealth(0.0F);
					}
				});
			}
		}
	}
}
