package net.domdiidom.jail.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import net.domdiidom.jail.main.Main;
import net.domdiidom.jail.utils.CreationState;

public class EventBlockPlace implements Listener	{
	Main main;
	public EventBlockPlace(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onEvent(BlockPlaceEvent event) {
		if(main.state == CreationState.CHEST && event.getBlock().getType() == Material.CHEST) {
			event.getPlayer().sendMessage("�3[JAIL] Bitte Doppelkiste erstellen!");
			main.state = CreationState.CHEST_2;
		}
		if(main.state == CreationState.CHEST_2 && event.getBlock().getType() == Material.CHEST) {
			int id = main.utils.getNextJail();
			main.getConfig().set("jail."+id+".chest.x", event.getBlock().getLocation().getBlockX());
			main.getConfig().set("jail."+id+".chest.y", event.getBlock().getLocation().getBlockY());
			main.getConfig().set("jail."+id+".chest.z", event.getBlock().getLocation().getBlockZ());
			
			main.getConfig().set("jail.value", id);
			main.saveConfig();
			
			event.getPlayer().getInventory().setItem(0, new ItemStack(main.getConfig().getInt(event.getPlayer().getName()+".item"), main.getConfig().getInt(event.getPlayer().getName()+".value")));
			
			event.getPlayer().sendMessage("�3[JAIL] Du bist fertig!");
						
			main.getConfig().set(event.getPlayer().getName(), null);
			main.saveConfig();
		}
	}
}
