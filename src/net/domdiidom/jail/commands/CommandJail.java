package net.domdiidom.jail.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.domdiidom.jail.main.Main;
import net.domdiidom.jail.utils.CreationState;


public class CommandJail implements CommandExecutor{
	Main main;
	public CommandJail(Main main) {
		this.main = main;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String name, String[] args) {
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("create")) {
				onCreateJail(sender, args);
				return true;
			}
		}else if(args.length >= 3) {
			try{onJailPlayer(sender, args);}catch(Exception e) {}
			return true;
		}else
			onMessage("�4WRONG ARGUMENTS!");
			
		return false;
	}
	
	private void onMessage(String msg) {
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage("�3[Jail] " + msg);
	}
	
	private void onCreateJail(CommandSender sender, String[] args) {
		try {
			Player player = (Player) sender;
			ItemStack item = new ItemStack(Material.STICK, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName("Jail Creation Tool");
			item.setItemMeta(meta);
			if(player.getInventory().getItem(0) == null) {
				main.getConfig().set(sender.getName()+".item", 0);
				main.getConfig().set(sender.getName()+".value", 0);
			}else {
				main.getConfig().set(sender.getName()+".item", player.getInventory().getItem(0).getTypeId());
				main.getConfig().set(sender.getName()+".value", player.getInventory().getItem(0).getAmount());
			}
			player.getInventory().setItem(0, item);
			main.saveConfig();
		
			sender.sendMessage("�3[JAIL] Setze den Spawn Punkt.");
		
			main.state = CreationState.SPAWN;
		}catch(Exception e) {}
	}
	
	private void onJailPlayer(CommandSender sender, String[] args) {		
		
		Player target = Bukkit.getPlayer(args[0]);
		
		target.sendMessage("�4[JAIL] Du wurdest eingesperrt!");
		
		if(main.utils.isCaptured(target)) {
			sender.sendMessage("�4[JAIL] Der Spieler ist bereits im Jail!");
			return;
		}
		int jail = 0;
		while(jail != main.utils.getHighestJail()){
			jail++;
			if(main.getConfig().getInt("jail."+jail+".occupied") == 0) {
				break;
			}
			if(jail == main.utils.getHighestJail()) {
				sender.sendMessage("�4[JAIL] Alle Zellen sind besetzt!");
				return;
			}
		}
		
		target.sendMessage("�4[JAIL] Dein Gef�ngnis ist: " + jail);
		String reason = "";
		for(int i=2; i < args.length; i++) {
			reason += args[i];
			reason += " ";
		}		
		
		main.getConfig().set("prisoners."+ target.getUniqueId()+".start.date", main.utils.getDate());
		main.getConfig().set("prisoners."+ target.getUniqueId()+".start.time", main.utils.getTime());
		main.getConfig().set("prisoners."+ target.getUniqueId()+".duration", Integer.valueOf(args[1])*60);
		main.getConfig().set("prisoners."+ target.getUniqueId()+".reason", reason);
		System.out.println("TEST");
		int x = main.getConfig().getInt("jail."+jail+".spawn.x");
		int y = main.getConfig().getInt("jail."+jail+".spawn.y");
		int z = main.getConfig().getInt("jail."+jail+".spawn.z");
		
		target.teleport(new Location(target.getWorld(), x, y, z));
		
		int _x = main.getConfig().getInt("jail."+jail+".chest.x");
		int _y = main.getConfig().getInt("jail."+jail+".chest.y");
		int _z = main.getConfig().getInt("jail."+jail+".chest.z");
		System.out.println("TEST1");
		Location location = new Location(target.getWorld(), _x, _y, _z);
		System.out.println("TEST2");
		Chest chest = (Chest) location.getBlock().getState();
		System.out.println("TEST3");
		try {
			ItemStack[] items = target.getInventory().getContents();
			for(int i=0; i<items.length; i++) {
				if(items[i] != null) {
					chest.getInventory().addItem(items[i]);
					target.getInventory().setItem(i, new ItemStack(Material.AIR));
				}
				System.out.println("SCHLEIFE 1");
			}
			System.out.println("TEST SCHLEIFE 1");
		}catch(Exception e) {e.printStackTrace();}
		try {
			ItemStack[] storage = target.getInventory().getStorageContents();
			for(int i=0; i<storage.length; i++) {
				if(storage[i] != null) {
					chest.getInventory().addItem(storage[i]);
					target.getInventory().setItem(i, new ItemStack(Material.AIR));
				}
				System.out.println("SCHLEIFE 2");
			}
			System.out.println("TEST SCHLEIFE 2");
		}catch(Exception e) {e.printStackTrace();}
		try {
			ItemStack[] armors = target.getInventory().getArmorContents();
			for(int i=0; i<armors.length; i++) {
				if(armors[i] != null) {
					chest.getInventory().addItem(armors[i]);
					target.getInventory().setItem(i, new ItemStack(Material.AIR));
				}
				System.out.println("SCHLEIFE 3");
			}
			System.out.println("TEST SCHLEIFE 3");
		}catch(Exception e) {e.printStackTrace();}	
		
		main.getConfig().set("jail."+jail+".occupied", 1);
		main.getConfig().set("jail."+jail+".prisoner", args[0]);
		main.getConfig().set("prisoners."+target.getUniqueId()+".jail", jail);
		main.saveConfig();
	}

}
