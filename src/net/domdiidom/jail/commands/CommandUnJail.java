package net.domdiidom.jail.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.domdiidom.jail.main.Main;

public class CommandUnJail implements CommandExecutor{
	Main main;
	public CommandUnJail(Main main) {
		this.main = main;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String name, String[] args) {
		if(args.length == 1) {
			Player player = Bukkit.getPlayer(args[0]);
			if(main.utils.isCaptured(player)) {
				int jail = main.getConfig().getInt("prisoners."+player.getUniqueId()+".jail");
				main.getConfig().set("jail."+jail+".occupied", 0);
				main.getConfig().set("jail."+jail+".prisoner", null);
				main.getConfig().set("prisoners."+player.getUniqueId(), null);
				main.saveConfig();
				return true;
			}
		}
		return false;
	}
}
