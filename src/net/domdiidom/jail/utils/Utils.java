package net.domdiidom.jail.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import net.domdiidom.jail.main.Main;
import net.minecraft.server.v1_11_R1.EntityInsentient;
import net.minecraft.server.v1_11_R1.Navigation;
import net.minecraft.server.v1_11_R1.PathEntity;

public class Utils {
	Main main;
	public Utils(Main main) {
		this.main = main;
	}
	
	public int getNextJail() {
		int highest = main.getConfig().getInt("jail.value") + 1;
		return highest;
	}
	
	public int getHighestJail() {
		int highest = main.getConfig().getInt("jail.value");
		return highest;
	}
	
	public String getDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		return format.format(date);
	}
	
	public String getTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(date);
	}
	
	public boolean isCaptured(Player player) {
		if(main.getConfig().getString("prisoners."+player.getUniqueId()) != null)
			return true;
		else
			return false;
	}
	
	public int getJail(Player player) {
		int jail = -1;
		if(isCaptured(player)) {
			jail = main.getConfig().getInt("prisoners."+player.getUniqueId()+".jail");
		}
		return jail;
	}
	
	public void addPrisoner(Player player) {
		List<String> list = new LinkedList<String>();
		list.clear();
		main.getConfig().getList("prisoners.list");
		list.add(player.getName());
		main.getConfig().set("prisoners.list", list);
		main.saveConfig();
		main.prisoners.add(player.getName());
	}
	
	public void removePrisoner(Player player) {
		List<String> list = new LinkedList<String>();
		list.clear();
		main.getConfig().getList("prisoners.list");
		list.remove(player.getName());
		main.getConfig().set("prisoners.list", list);
		main.saveConfig();
		main.prisoners.remove(player.getName());
	}
	
	public void moveEntity(LivingEntity e,Location l,float speed){
        EntityInsentient ei = ((EntityInsentient)((CraftEntity) e).getHandle());
        Navigation nav = (Navigation) ((EntityInsentient)((CraftEntity) e).getHandle()).getNavigation();
        nav.a(true);
        PathEntity path = nav.a(l.getX(),l.getY(),l.getZ());
        nav.a(path, speed);
    }
}
